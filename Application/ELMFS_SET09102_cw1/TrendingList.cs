﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ELMFS_SET09102_cw1
{
    class TrendingList
    {
        private SIRList[] SIRItems = new SIRList[100];
        private QuarantineList[] QItems = new QuarantineList[100];
        private HashtagList[] HTItems = new HashtagList[100];
        private MentionsList[] MItems = new MentionsList[100];
        private int SIRcount = 0;
        private int QCount = 0;
        private int HTCount = 0;
        private int MCount = 0;


        public void ThisSIR(SIRList nSIRList)
        {
            if (SIRcount >= SIRItems.Length)
            {
                MessageBox.Show("Error - Could not add to SIR report!");
            }
            else
            {
                SIRItems[SIRcount] = nSIRList;
                SIRcount++;
            }
        }
        public void ThisQuarentine(QuarantineList nQList)
        {
            if (QCount >= QItems.Length)
            {
                MessageBox.Show("Error - Could not add to SIR report!");
            }
            else
            {
                QItems[QCount] = nQList;
                QCount++;
            }
        }
        public void ThisHashTag(HashtagList nHTList)
        {
            if (HTCount >= HTItems.Length)
            {
                MessageBox.Show("Error - Could not add to SIR report!");
            }
            else
            {
                HTItems[HTCount] = nHTList;
                HTCount++;
            }
        }
        public void ThisMention(MentionsList nMList)
        {
            if (MCount >= MItems.Length)
            {
                MessageBox.Show("Error - Could not add to SIR report!");
            }
            else
            {
                MItems[MCount] = nMList;
                MCount++;
            }
        }

        public void printTrendingList()
        {
            for(int x = 0; x < SIRcount; x++)
            {
                SIRList SIR = SIRItems[x];
                MessageBox.Show("Sports Center Code - " + SIR.SCC + " \n " + "Nature of Incedent - " + SIR.NoI);
            }
            for (int x = 0; x < HTCount; x++)
            {
                HashtagList HT = HTItems[x];
                MessageBox.Show("Hashtags in tweet - " + HT.Hashtags);
            }
            for (int x = 0; x < MCount; x++)
            {
                MentionsList Mention = MItems[x];
                MessageBox.Show("Mentions in tweet - " + Mention.Mentions);
            }
        }
    }
}
