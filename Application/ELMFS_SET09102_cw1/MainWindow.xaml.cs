﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
namespace ELMFS_SET09102_cw1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Direct_Input_Click(object sender, RoutedEventArgs e)
        {
            FileStream clearMentionsFile = File.Open(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Mentions.txt", FileMode.Open);
            clearMentionsFile.SetLength(0);
            clearMentionsFile.Close();
            FileStream clearHashtagsFile = File.Open(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Hashtags.txt", FileMode.Open);
            clearHashtagsFile.SetLength(0);
            clearHashtagsFile.Close();
            FileStream ClearSIR = File.Open(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\SIR.txt", FileMode.Open);
            ClearSIR.SetLength(0);
            ClearSIR.Close();
            Direct_Input_Window DIW = new Direct_Input_Window();
            DIW.Show();
            this.Close();
        }

        /*private void Load_Input_Click(object sender, RoutedEventArgs e)
        {
            Load_Input_Window LIW = new Load_Input_Window();
            LIW.Show();
            this.Close();
        }*/

        private void Log_output_Click(object sender, RoutedEventArgs e)
        {
            Trending_Log TL = new Trending_Log();
            TL.Show();
            this.Close();
        }
    }
}
