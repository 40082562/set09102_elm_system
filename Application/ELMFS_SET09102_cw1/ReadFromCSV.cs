﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMFS_SET09102_cw1
{
    class ReadFromCSV
    {
        static void RMain(string[] args)
        {
            List<TextSpeak> TxtSpk = File.ReadAllLines(@"C:\Users\Ben\Documents\set09102_elm_system\textwords.csv")
                .Skip(1)
                .Select(t => TextSpeak.InCsv(t))
                .ToList();
        }
    }
    class TextSpeak
    {
        string Abreviated;
        string Expanded;


        public static TextSpeak InCsv(string csvLine)
        {
            string[] TxtSpk = csvLine.Split(',');
            TextSpeak textSpeak = new TextSpeak();
            textSpeak.Abreviated = TxtSpk[0];
            textSpeak.Expanded = TxtSpk[1];
            return (textSpeak);
        }
    
    }
}
