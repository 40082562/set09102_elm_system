﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ELMFS_SET09102_cw1
{
    public class SIRList
    {
        public class SIR
        {
            public string SCC;
            public string NoI;
        }

        private string _SCC;
        private List<SIR> _SCC_LIST = new List<SIR>();


        private string _NoI; 

        public string SCC
        {
            get
            {
                return _SCC;
            }
            set
            {
                _SCC = value;
            }
        }
        public string NoI
        {
            get
            {
                return _NoI;
            }
            set
            {
                _NoI = value;
            }
        }
    }

}
