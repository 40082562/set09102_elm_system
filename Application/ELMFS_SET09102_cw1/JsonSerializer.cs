﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.Serialization;

namespace ELMFS_SET09102_cw1
{
    [DataContract]
    public class JsonSerializer
    {
        [DataMember]
        public string message_ID;

        [DataMember]
        public string message_Body;
    }
}
