﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace ELMFS_SET09102_cw1
{
    /// <summary>
    /// Interaction logic for Direct_Input_Window.xaml
    /// </summary>
    public partial class Direct_Input_Window : Window
    {
        public Direct_Input_Window()
        {
            InitializeComponent();
        }

        private void Return_Home_BTN_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MW = new MainWindow();
            MW.Show();
            this.Close();
        }

        private void Analyze_BTN_Click(object sender, RoutedEventArgs e)
        {
            List<string> sirReportList = new List<string>();
            List<string> HashtagList = new List<string>();
            List<string> MentionsList = new List<string>();
            List<string> URLS = new List<string>();
            HashtagList TrendList = new HashtagList();
            //converts ascii to text ... thought this was a requirement
            //=================ASCII converter===========================
            /*string asciioutput = " ";
            string asciiinput = Text_Input_TB.Text;
            char[] asciiseperator = { ' ' };
            string[] sortedascii = asciiinput.Split(asciiseperator);
            foreach (string asciichars in sortedascii)
            {
                int i = int.Parse(asciichars);
                char convert = (char)i;
                string asciitext = convert.ToString();
                asciioutput += asciitext;
            }
            MessageBox.Show(asciioutput);*/
            //===========================================================

            //Implements Dictionary
            var fileParser = new FileParser(@"C:\Users\Ben\Documents\set09102_elm_system\textwords.csv"); //home

            //uni file loaction
            //var fileParser = new FileParser(@"C:\Users\40082562\Documents\set09102_elm_system\textwords.csv");

            fileParser.Parse();

            //handles the splitting of the lines
            string[] File = Text_Input_TB.Text.Split(' ', '\n');
            //foreach(string s in isInFile){ Console.WriteLine(s); }

            //Splits the string int seperate words
            char[] stringSep = { ',', ' ', '!', '?', '.', ':', ';' };
            //string builder for the files
            StringBuilder TXTSB = new StringBuilder();
            for (int i = 0; i < File.Length; i++)
            {
                TXTSB.Append(File[i] + " ");
            }
            // formats multiline text into one string then tranfers each value to sting[]
            string unedited = TXTSB.ToString();
            string edited = Regex.Replace(unedited, @"\t|\r|\n", " ");
            string inMessage = edited.ToString();
            string[] isInFile = inMessage.Split(stringSep);
            Console.WriteLine(inMessage);
            //=================== For Ascii conversion ==================
            //MessageBox.Show(isInFile.Length.ToString());
            //MessageBox.Show(isInFile[1]);
            //===========================================================
            var firstWord = isInFile.First();
            string a = firstWord.ToString();

            //SMS handling
            if (Regex.IsMatch(a, "^[Ss]{1}[0-9]{9}$"))
            {
                StringBuilder expandedTXT = new StringBuilder(inMessage);
                MessageBox.Show(a + "Message Type: SMS");
                foreach (string s in isInFile)
                {
                    //test for reading words from TB 
                    //Console.WriteLine(s);
                    if (fileParser.IsWordAvailable(s))
                    {
                        Console.WriteLine(s);// displays word checked in console
                        foreach (var word in fileParser.WordDictionary)
                        {
                            expandedTXT.Replace(word.Key, String.Format("{0} <{1}>", word.Key, word.Value));
                        }
                        MessageBox.Show("New String: " + expandedTXT.ToString());
                    }
                }
                JsonSerializer serializeSMS = new JsonSerializer()
                {
                    message_ID = a,
                    message_Body = expandedTXT.ToString()
                };

                DataContractJsonSerializer JS = new DataContractJsonSerializer(typeof(JsonSerializer));
                MemoryStream MS = new MemoryStream();
                JS.WriteObject(MS, serializeSMS);
                Console.WriteLine("Json text");
                MS.Position = 0;
                StreamReader sr = new StreamReader(MS);
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
                MS.Close();
            }


            //Email handling
            if (Regex.IsMatch(a, "^[Ee]{1}[0-9]{9}$"))
            {
                MessageBox.Show(a + "Message Type: Email");
                var subject = isInFile[2];
                string SubTXT = subject.ToString();


                SIRList sirList = new SIRList();
                TrendingList addToTrending = new TrendingList();
                //finds email address in email body
                Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
                    RegexOptions.IgnoreCase);
                //finds paterns that match that of an email address
                MatchCollection emailMatches = emailRegex.Matches(inMessage);

                StringBuilder emailString = new StringBuilder();
                //writes these to string
                foreach (Match emailMatch in emailMatches)
                {
                    emailString.AppendLine(emailMatch.Value);
                }
                //finds and replaces URLS
                StringBuilder URLQuar = new StringBuilder(inMessage);
                Regex regexURL = new Regex("(http|https)://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);
                MatchCollection URLMatches = regexURL.Matches(inMessage);
                foreach (Match match in URLMatches)
                {
                    URLS.Add(match.Value);
                    URLQuar.Replace(match.Value, "<URL Quarantined> ");
                }
                //MessageBox.Show(URLQuar.ToString());
                //gets SIRreport details
                //MessageBox.Show(emailString.ToString());
                //looks for match for SIR 
                if (Regex.IsMatch(SubTXT, "^[Ss]{1}[Ii]{1}[Rr]{1}$"))
                {
                    sirList.SCC = isInFile[9].ToString();
                    //sirList.NoI = isInFile[15].ToString();
                    if(isInFile[15].Contains("Theft"))
                    {
                        sirList.NoI = "Theft of Property";
                    }
                    else if (isInFile[15].Contains("Staff"))
                    {
                        sirList.NoI = "Staff Attack";
                    }
                    else if (isInFile[15].Contains("Device"))
                    {
                        sirList.NoI = "Device Damage";
                    }
                    else if (isInFile[15].Contains("Raid"))
                    {
                        sirList.NoI = "Raid";
                    }
                    else if (isInFile[15].Contains("Customer"))
                    {
                        sirList.NoI = "Customer Attack";
                    }
                    else if (isInFile[15].Contains("Bomb"))
                    {
                        sirList.NoI = "Bomb Threat";
                    }
                    else if (isInFile[15].Contains("Terrorism"))
                    {
                        sirList.NoI = "Terrorism";
                    }
                    else if (isInFile[15].Contains("Suspicious"))
                    {
                        sirList.NoI = "Suspicious Incedent";
                    }
                    else if (isInFile[15].Contains("Sport"))
                    {
                        sirList.NoI = "Sports Injury";
                    }
                    else if (isInFile[15].Contains("Personal"))
                    {
                        sirList.NoI = "Personal Info Leak";
                    }
                    //addToTrending.ThisSIR(sirList);
                    sirReportList.Add(sirList.SCC);
                    sirReportList.Add(sirList.NoI);

                }
                JsonSerializer serializeEmail = new JsonSerializer()
                {
                    message_ID = a,
                    message_Body = URLQuar.ToString()
                };

                DataContractJsonSerializer JS = new DataContractJsonSerializer(typeof(JsonSerializer));
                MemoryStream MS = new MemoryStream();
                JS.WriteObject(MS, serializeEmail);
                Console.WriteLine("Json text");
                MS.Position = 0;
                StreamReader sr = new StreamReader(MS);
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
                MS.Close();
                //addToTrending.printTrendingList();
            }


            //Tweet handling
            if (Regex.IsMatch(a, "^[Tt]{1}[0-9]{9}$"))
            {
                MessageBox.Show(a + "Message Type: Tweet");
                StringBuilder expandedTXT = new StringBuilder(inMessage);
                HashtagList HTList = new HashtagList();
                MentionsList Ment = new MentionsList();
                TrendingList trendingList = new TrendingList();
                Regex TwitterHandle = new Regex(@"@\w+([-+.]\w+)*", RegexOptions.IgnoreCase);
                MatchCollection THMatches = TwitterHandle.Matches(inMessage);
                StringBuilder THString = new StringBuilder();
                foreach (Match Twithand in THMatches)
                {
                    THString.AppendLine(Twithand.Value);
                    //Ment.Mentions = Twithand.Value;
                    //trendingList.ThisMention(Ment);
                    MentionsList.Add(Twithand.Value);
                    //MentionFile.WriteLine(Twithand.Value.ToString() + "\n");
                }

                //MessageBox.Show(THString.ToString());
                foreach (string n in isInFile)
                {

                    //test for reading words from TB 
                    //Console.WriteLine(s);
                    if (fileParser.IsWordAvailable(n))
                    {
                        Console.WriteLine(n);// displays word checked in console
                        foreach (var word in fileParser.WordDictionary)
                        {
                            expandedTXT.Replace(word.Key, String.Format("{0} <{1}>", word.Key, word.Value));
                        }
                    }
                    if (n.StartsWith("#"))
                    {
                        //HTList.Hashtags = n.ToString();
                        //trendingList.ThisHashTag(HTList);
                        HashtagList.Add(n.ToString());
                    }
                }
                JsonSerializer serializeTweet = new JsonSerializer()
                {
                    message_ID = a,
                    message_Body = expandedTXT.ToString()
                };

                DataContractJsonSerializer JS = new DataContractJsonSerializer(typeof(JsonSerializer));
                MemoryStream MS = new MemoryStream();
                JS.WriteObject(MS, serializeTweet);
                Console.WriteLine("Json text");
                MS.Position = 0;
                StreamReader sr = new StreamReader(MS);
                Console.WriteLine(sr.ReadToEnd());
                sr.Close();
                MS.Close();
            }
            //trendingList.printTrendingList();
        
    
            //hashtags in text to string 
            var HTIS = string.Join(" ", HashtagList.ToArray());
            string Hashtagsintext = HTIS.ToString();
            //Mentions in text to string
            var MIS = string.Join(" ", MentionsList.ToArray());
            string Mentionsintext = MIS.ToString();
            //SIRS in text to string
            var SIRIS = string.Join(" ", sirReportList.ToArray());
            string SIRintext = SIRIS.ToString();
            //URLs in text to string
            var URLIS = string.Join(" ", URLS.ToArray());
            string URLInText = URLIS.ToString();
            //test of output.
            //MessageBox.Show(SIRintext + " " + Mentionsintext + " " + Hashtagsintext);
            //output for 
            using (StreamWriter MentionFile = new StreamWriter(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Mentions.txt"))
            {
                MentionFile.WriteLine(Mentionsintext + "\n");
            }
            using (StreamWriter HashtagFile = new StreamWriter(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Hashtags.txt"))
            {
                HashtagFile.WriteLine(Hashtagsintext + "\n");
            }
            using (StreamWriter SIRFile = new StreamWriter(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\SIR.txt"))
            {
                SIRFile.WriteLine(SIRintext + "\n");
            }
            using (StreamWriter URLFile = new StreamWriter(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\URLS.txt"))
            {
                URLFile.WriteLine(URLS + "\n");
            }

            Trending_Log TL = new Trending_Log();
            TL.Show();
        }
    }
}
