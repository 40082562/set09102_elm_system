﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ELMFS_SET09102_cw1
{
    /// <summary>
    /// Interaction logic for Load_Input_Window.xaml
    /// </summary>
    public partial class Load_Input_Window : Window
    {
        public Load_Input_Window()
        {
            InitializeComponent();
        }

        private void Select_File_BTN_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog OF = new Microsoft.Win32.OpenFileDialog();

            OF.DefaultExt = ".*";


            Nullable<bool> result = OF.ShowDialog();

            if (result == true)
            {
                string filename = OF.FileName;
                File_Path_TB.Text = filename;
            }
        }



        private void Load_File_Contents_BTN_Click(object sender, RoutedEventArgs e)
        {
            string FilePath = File_Path_TB.Text;

            //Implements Dictionary
            var fileParser = new FileParser(@"C:\Users\Ben\Documents\set09102_elm_system\textwords.csv");
            //uni file loaction
            //var fileParser = new FileParser(@"C:\Users\40082562\Documents\set09102_elm_system\textwords.csv");
            fileParser.Parse();
            //handles reading, splitting and searching of file
            FileStream inFile = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            StreamReader readFile = new StreamReader(inFile);

            char[] stringSep = { ',', ' ', '!', '?', '.' };

            string[] isInFile = readFile.ReadToEnd().Split(stringSep);
            string apendfile = readFile.ToString();
            StringBuilder SB = new StringBuilder(apendfile);

            foreach (string s in isInFile)
            { 
                if (fileParser.IsWordAvailable(s))
                {
                    Console.WriteLine(s);// displays word checked in console
                    MessageBox.Show("the word "+ s + " was found in the text");
                    //replace words in string 
                    foreach(KeyValuePair<string, string> rplace in fileParser.WordDictionary)
                    {
                        string RPText = SB.Replace(rplace.Key, rplace.Key + " (" + rplace.Value + ")").ToString();
                    }

                }
                else
                {
                    if (s.StartsWith("#"))
                    {
                        //Console.WriteLine(s);// displays word checked in console
                        MessageBox.Show(s);
                    }
                }
            }

            Console.WriteLine(SB.ToString());
        }

        /*string inTxtFile = readFile.ReadLine();
        while ((inTxtFile = readFile.ReadLine()) != null)
        {
            var strWord = inTxtFile.Split(stringSep);  
        }*/
        //string combineList = string.Join(",", isInFile.ToArray());
        //Console.WriteLine(combineList.ToString());
        /*foreach (string i in isInFile)
        {
            Console.WriteLine(i.ToString());
        }*/

        /*if (fileParser.IsWordAvailable(isInFile.ToString()))
        {
            MessageBox.Show("Match found" + isInFile.ToString());
        }
        else
        {
            MessageBox.Show("No Match" + isInFile.ToString());
        }*/



        private void Return_Home_BTN_Click(object sender, RoutedEventArgs e)
        {
            MainWindow MW = new MainWindow();
            MW.Show();
            this.Close();
        }
    }
}

