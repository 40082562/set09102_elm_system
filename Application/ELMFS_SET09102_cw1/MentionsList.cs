﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMFS_SET09102_cw1
{
    class MentionsList
    {
        private string _mentions; 
        public string Mentions
        {
            get
            {
                return _mentions;
            }
            set
            {
                _mentions = value;
            }
        }
    }
}
