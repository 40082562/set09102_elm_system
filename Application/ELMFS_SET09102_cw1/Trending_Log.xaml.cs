﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ELMFS_SET09102_cw1
{
    /// <summary>
    /// Interaction logic for Trending_Log.xaml
    /// </summary>
    public partial class Trending_Log : Window
    {
        public Trending_Log()
        {
            InitializeComponent();
            using (var MentionsOutputText = new StreamReader(File.OpenRead(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Mentions.txt")))
            {
                MentionsOutput.Text = MentionsOutputText.ReadToEnd();
                MentionsOutputText.Dispose();
            }
            using (var HashtagsOutputText = new StreamReader(File.OpenRead(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\Hashtags.txt")))
            {
                HashtagOutput.Text = HashtagsOutputText.ReadToEnd();
                HashtagsOutputText.Dispose();
            }
            using (var SIROutputText = new StreamReader(File.OpenRead(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\SIR.txt")))
            {
                SIROutput.Text = SIROutputText.ReadToEnd();
                SIROutputText.Dispose();
            }
            using (var URLOutputText = new StreamReader(File.OpenRead(@"C:\Users\Ben\Documents\set09102_elm_system\Application\ELMFS_SET09102_cw1\URLS.txt")))
            {
                URLOutput.Text = URLOutputText.ReadToEnd();
                URLOutputText.Dispose();
            }
        }
        private void Return_Home_BTN_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
