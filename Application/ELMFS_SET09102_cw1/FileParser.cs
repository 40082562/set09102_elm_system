﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ELMFS_SET09102_cw1
{
    class FileParser
    {
        internal Dictionary<string, string> WordDictionary;

        private string _filePath;
        char[] Seperators = { ',' };
        internal FileParser(string filePath)
        {
            _filePath = filePath;
            WordDictionary = new Dictionary<string, string>();

        }


        internal void Parse()
        {
            StreamReader sr = new StreamReader(_filePath);
            string TxtWrd = sr.ReadLine();
            while ((TxtWrd = sr.ReadLine()) != null)
            {
                var Words = TxtWrd.Split(Seperators, StringSplitOptions.None);
                if (!WordDictionary.ContainsKey(Words[0]))
                    WordDictionary.Add(Words[0], Words[1]);
            }

            Console.WriteLine(WordDictionary);
            foreach (KeyValuePair<string, string> i in WordDictionary)
            {
                Console.WriteLine(i.Key + " " + i.Value);
            }
        }

        //pulls all key values from dictionary
        public bool IsWordAvailable(string word)
        {
            return WordDictionary.ContainsKey(word);
        }
    }
}
