﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
 * ToDo List for Class Message_filter
 * 
 * Filter message to type
 * Tweet;
 *  Header - @ + 15 chars max
 *  Body - 140 chars max
 * Email;
 *  Sender - first last firstlast@domain
 *  Header - 20 cahrs
 *  Body - 1028 chars max, will contain links
 * SMS;
 *  Sender - international phone number 07 or +44 etc
 *  Body - 140 chars max, will contain txtspeak
 *  
 *  process messages as appropriate
 */
namespace ELMFS_SET09102_cw1
{
    public class HashtagList
    {
        private string _Hashtags;

        public string Hashtags
        {
            get
            {
                return _Hashtags;
            }
            set
            {
                _Hashtags = value;
            }
        }
    }
}